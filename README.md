Collections:
check_point.mgmt
check_point.gaia

---
- name: playbook name
  hosts: check_point
  gather_facts: false
  connection: httpapi
  tasks:
    - name: task to have network
      check_point.mgmt.cp_mgmt_network:
        name: "network name"
        subnet: "4.1.76.0"
        mask_length: 24
        auto_publish_session: true

      vars: 
        ansible_checkpoint_domain: "SMC User"
		
[check_point:vars]
ansible_httpapi_use_ssl=True
ansible_httpapi_validate_certs=False
#ansible_user=%CHECK_POINT_MANAGEMENT_SERVER_USER%
#ansible_password=%CHECK_POINT_MANAGEMENT_SERVER_PASSWORD%
ansible_network_os=check_point.mgmt.checkpoint

Ansible Credential type

INPUT CONFIGURATION
fields:
  - id: ansible_checkpoint_domain
    type: string
    label: domain
  - id: ansible_api_key
    type: string
    label: api_key
	secret: true
required:
  - ansible_checkpoint_domain
  - ansible_api_key
  
  
INJECTOR CONFIGURATION
extra_vars:
  ansible_api_key: '{{ ansible_api_key }}'
  ansible_checkpoint_domain: '{{ ansible_checkpoint_domain }}'
